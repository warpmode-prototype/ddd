package com.example.ddd
package domain

import cats.implicits._
import eu.timepit.refined.api.{RefinedType, RefinedTypeOps}

abstract class CustomRefinedTypeOps[FTP, T, E](f: String => E)(implicit rt: RefinedType.AuxT[FTP, T]) {
  private val ops = new RefinedTypeOps[FTP, T]()

  def from(t: T): Either[E, FTP] = ops.from(t).leftMap(f)

  def unapply(t: T): Option[FTP] = from(t).toOption

  def unsafeFrom(t: T): FTP = ops.unsafeFrom(t)
}
