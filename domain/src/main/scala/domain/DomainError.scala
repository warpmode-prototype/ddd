package com.example.ddd
package domain

import cats.data.NonEmptyChain

sealed trait DomainError extends Product with Serializable

case object InvalidPersistedData extends DomainError

final case class OrderTotalExceedsApprovedLimit[CurrencyCode <: String](
  total: Money[CurrencyCode],
  limit: ApprovedLimit[CurrencyCode],
) extends DomainError

final case class InvalidApprovedLimit(error: String) extends DomainError

final case class InvalidCurrency(currencyCode: String) extends DomainError

final case class InvalidDescription(error: String) extends DomainError

final case class InvalidPartNumber(error: String) extends DomainError

final case class InvalidQuantity(error: String) extends DomainError

case object CatalogPartNotFound extends DomainError

object DomainError {
  type DomainErrors = NonEmptyChain[DomainError]
}
