package com.example.ddd
package domain

import eu.timepit.refined.api.Refined

import cats.Eq

package object util {

  @SuppressWarnings(Array("org.wartremover.warts.Equals"))
  implicit def eqRefined[A, B]: Eq[Refined[A, B]] = (x: Refined[A, B], y: Refined[A, B]) => x.value == y.value

}
