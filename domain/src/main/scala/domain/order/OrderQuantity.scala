package com.example.ddd
package domain.order

import domain.catalog.CatalogPart.PartNumber
import domain.order.LineItem.Quantity

final case class OrderQuantity(partNumber: PartNumber, quantity: Quantity)
