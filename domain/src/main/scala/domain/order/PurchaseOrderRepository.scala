package com.example.ddd
package domain.order

import domain.catalog.CatalogPart.PartNumber
import domain.order.PurchaseOrder.Id

trait PurchaseOrderRepository[F[_], Key] {
  def nextIdentity: F[Id[Key]]

  def exists(id: Id[Key]): F[Boolean]

  def findOne(id: Id[Key]): F[Option[PurchaseOrder[Key, _ <: String]]]

  def findByCatalogPart(partNumber: PartNumber): F[Seq[PurchaseOrder[Key, _ <: String]]]

  def findAll: F[Seq[PurchaseOrder[Key, _ <: String]]]

  def count: F[Long]

  def save(order: PurchaseOrder[Key, _ <: String]): F[Unit]

  def delete(id: Id[Key]): F[Unit]
}
