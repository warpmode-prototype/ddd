package com.example.ddd
package domain.order

import domain.DomainError.DomainErrors
import domain.catalog.CatalogPart.PartNumber
import domain.catalog.CatalogPartRepository
import domain.order.LineItem.Quantity
import domain.{ApprovedLimit, CatalogPartNotFound, DomainError, InvalidCurrency}

import cats.Monad
import cats.data.{EitherNec, EitherT, NonEmptyChain, NonEmptyList, OptionT}
import cats.implicits._

final class PurchaseOrderFactory[F[_]: Monad, Key](
  catalogPartRepository: CatalogPartRepository[F, Key],
  purchaseOrderConfiguration: PurchaseOrderConfiguration,
  purchaseOrderRepository: PurchaseOrderRepository[F, Key],
) {

  def create[CurrencyCode <: String: ValueOf](
    quantities: NonEmptyList[OrderQuantity]
  ): F[EitherNec[DomainError, PurchaseOrder[Key, CurrencyCode]]] =
    (
      EitherT.fromEither[F](getApprovedLimit).leftWiden[DomainErrors],
      EitherT(createLineItems(quantities)).leftWiden[DomainErrors],
      EitherT.right[DomainErrors](purchaseOrderRepository.nextIdentity),
    ).parMapN { (approvedLimit, lineItems, id) =>
      EitherT.fromEither[F](PurchaseOrder(id, 1, approvedLimit, lineItems).leftWiden[DomainErrors])
    }.flatten
      .value

// Monadic composition (returns first error only)
//    (for {
//      approvedLimit <- EitherT.fromEither[F](getApprovedLimit)
//      lineItems     <- EitherT(createLineItems(quantities))
//      id            <- EitherT.right(purchaseOrderRepository.nextIdentity())
//      order         <- EitherT.fromEither[F](PurchaseOrder(id, 1, approvedLimit, lineItems)).leftWiden[DomainErrors]
//    } yield order).value

  private def getApprovedLimit[CurrencyCode <: String: ValueOf]
    : EitherNec[InvalidCurrency, ApprovedLimit[CurrencyCode]] =
    Either.fromOption(
      purchaseOrderConfiguration.getApprovedLimit,
      NonEmptyChain.one(InvalidCurrency(valueOf[CurrencyCode])),
    )

  private def createLineItems[CurrencyCode <: String: ValueOf](
    quantities: NonEmptyList[OrderQuantity]
  ): F[EitherNec[CatalogPartNotFound.type, NonEmptyList[LineItem[CurrencyCode]]]] =
    for {
      maybeLineItems <- quantities.traverse(quantity =>
        createLineItem(quantity.partNumber, quantity.quantity).map(_.toEitherNec)
      )
    } yield maybeLineItems.parSequence

  private def createLineItem[CurrencyCode <: String: ValueOf](
    partNumber: PartNumber,
    quantity: Quantity,
  ): F[Either[CatalogPartNotFound.type, LineItem[CurrencyCode]]] =
    OptionT(catalogPartRepository.findByPartNumber(partNumber))
      .toRight(CatalogPartNotFound)
      .map { catalogPart =>
        LineItem[CurrencyCode](
          catalogPart.partNumber,
          catalogPart.description,
          quantity,
          catalogPart.price,
        )
      }
      .value

}
