package com.example.ddd
package domain.order

import domain.catalog.CatalogPart.{Description, PartNumber}
import domain.order.LineItem.Quantity
import domain.{CustomRefinedTypeOps, InvalidQuantity, Money}

import eu.timepit.refined.api._
import eu.timepit.refined.numeric.Positive

final case class LineItem[CurrencyCode <: String: ValueOf](
  partNumber: PartNumber,
  description: Description,
  quantity: Quantity,
  price: Money[CurrencyCode],
) {
  def subTotal: Money[CurrencyCode] = Money.unsafeFrom[CurrencyCode](quantity.value * price.amount)
}

object LineItem {
  type Quantity = Int Refined Positive
  object Quantity extends CustomRefinedTypeOps[Quantity, Int, InvalidQuantity](InvalidQuantity)
}
