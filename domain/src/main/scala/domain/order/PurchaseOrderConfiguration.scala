package com.example.ddd
package domain.order

import domain.ApprovedLimit

final case class PurchaseOrderConfiguration(approvedLimitEUR: ApprovedLimit["EUR"]) {

  @SuppressWarnings(Array("AsInstanceOf", "org.wartremover.warts.AsInstanceOf"))
  def getApprovedLimit[CurrencyCode <: String: ValueOf]: Option[ApprovedLimit[CurrencyCode]] =
    valueOf[CurrencyCode] match {
      case "EUR" => Some(approvedLimitEUR.asInstanceOf[ApprovedLimit[CurrencyCode]])
      case _     => None
    }

}
