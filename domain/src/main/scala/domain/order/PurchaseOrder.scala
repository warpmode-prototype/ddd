package com.example.ddd
package domain.order

import domain.order.PurchaseOrder.Id
import domain.{ApprovedLimit, DomainError, Money, OrderTotalExceedsApprovedLimit}

import cats.data.{EitherNec, NonEmptyChain, NonEmptyList}
import cats.implicits._

sealed abstract case class PurchaseOrder[Key, CurrencyCode <: String: ValueOf](
  id: Id[Key],
  version: Long,
  approvedLimit: ApprovedLimit[CurrencyCode],
  lineItems: NonEmptyList[LineItem[CurrencyCode]],
) {

  def changeApprovedLimit(
    newApprovedLimit: ApprovedLimit[CurrencyCode]
  ): EitherNec[DomainError, PurchaseOrder[Key, CurrencyCode]] =
    PurchaseOrder(id, version, newApprovedLimit, lineItems)

  def totalPrice: Money[CurrencyCode] = PurchaseOrder.totalPrice(lineItems)
}

object PurchaseOrder {
  final case class Id[Key](value: Key) extends AnyVal

  def apply[Key, CurrencyCode <: String: ValueOf](
    id: Id[Key],
    version: Long,
    approvedLimit: ApprovedLimit[CurrencyCode],
    lineItems: NonEmptyList[LineItem[CurrencyCode]],
  ): Either[NonEmptyChain[OrderTotalExceedsApprovedLimit[CurrencyCode]], PurchaseOrder[Key, CurrencyCode]] =
    if (totalPrice(lineItems).amount <= approvedLimit.value.amount)
      new PurchaseOrder(id, version, approvedLimit, lineItems) {}.asRight
    else
      OrderTotalExceedsApprovedLimit(totalPrice(lineItems), approvedLimit).asLeft.toEitherNec

  private def totalPrice[CurrencyCode <: String: ValueOf](
    lineItems: NonEmptyList[LineItem[CurrencyCode]]
  ): Money[CurrencyCode] =
    lineItems.map(_.subTotal).toList.combineAll

}
