package com.example.ddd

import cats.implicits._
import eu.timepit.refined.api.{Refined, RefinedTypeOps}
import eu.timepit.refined.numeric.Positive

package object domain {
  type ApprovedLimit[CurrencyCode <: String] = Money[CurrencyCode] Refined Positive

  object ApprovedLimit {

    def from[CurrencyCode <: String](
      value: Money[CurrencyCode]
    ): Either[InvalidApprovedLimit, ApprovedLimit[CurrencyCode]] =
      new RefinedTypeOps[ApprovedLimit[CurrencyCode], Money[CurrencyCode]] {}.from(value).leftMap(InvalidApprovedLimit)

  }

}
