package com.example.ddd
package domain

import domain.order.{PurchaseOrderConfiguration, PurchaseOrderFactory}

import distage.{ModuleDef, Tag, TagK}

@SuppressWarnings(Array("org.wartremover.warts.NonUnitStatements"))
final class DomainModule[F[_], Key: Tag, TX[_]: TagK] extends ModuleDef {
  make[PurchaseOrderConfiguration].from((configuration: DomainConfiguration) => configuration.purchaseOrder)

  make[PurchaseOrderFactory[TX, Key]]
}
