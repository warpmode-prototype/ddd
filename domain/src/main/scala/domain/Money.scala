package com.example.ddd
package domain

import cats.kernel.Monoid
import cats.syntax.either._
import eu.timepit.refined.api.Validate
import eu.timepit.refined.numeric.{Greater, Positive}

import java.util.Currency

sealed abstract case class Money[CurrencyCode <: String](amount: BigDecimal, currency: Currency)(implicit
  v: ValueOf[CurrencyCode]
) extends Ordered[Money[CurrencyCode]] {

  val currencyCode: CurrencyCode = v.value

  override def compare(that: Money[CurrencyCode]): Int = amount.compare(that.amount)
}

object Money {

  def from[CurrencyCode <: String](
    amount: BigDecimal
  )(implicit v: ValueOf[CurrencyCode]): Either[InvalidCurrency, Money[CurrencyCode]] =
    Either
      .catchNonFatal(new Money[CurrencyCode](amount, Currency.getInstance(v.value)) {})
      .leftMap(_ => InvalidCurrency(v.value))

  def unsafeFrom[CurrencyCode <: String](amount: BigDecimal)(implicit v: ValueOf[CurrencyCode]): Money[CurrencyCode] =
    new Money[CurrencyCode](amount, Currency.getInstance(v.value)) {}

  implicit def validatePositive[CurrencyCode <: String]: Validate[Money[CurrencyCode], Positive] =
    Validate.fromPredicate(m => m.amount >= 0, _ => "Money amount is positive", Greater(shapeless.nat._0))

  implicit def moneyMonoid[CurrencyCode <: String: ValueOf]: Monoid[Money[CurrencyCode]] =
    new Monoid[Money[CurrencyCode]] {
      def empty: Money[CurrencyCode] = Money.unsafeFrom[CurrencyCode](0)

      override def combine(x: Money[CurrencyCode], y: Money[CurrencyCode]): Money[CurrencyCode] =
        Money.unsafeFrom[CurrencyCode](x.amount + y.amount)

    }

}
