package com.example.ddd
package domain

import domain.order.PurchaseOrderConfiguration

final case class DomainConfiguration(purchaseOrder: PurchaseOrderConfiguration)
