package com.example.ddd
package domain.catalog

import domain.catalog.CatalogPart.{Description, PartNumber}
import domain.{CustomRefinedTypeOps, InvalidDescription, InvalidPartNumber, Money}

import eu.timepit.refined.api.Refined
import eu.timepit.refined.collection.NonEmpty

final case class CatalogPart[CurrencyCode <: String](
  partNumber: PartNumber,
  description: Description,
  price: Money[CurrencyCode],
)

object CatalogPart {
  type PartNumber = String Refined NonEmpty
  object PartNumber extends CustomRefinedTypeOps[Description, String, InvalidPartNumber](InvalidPartNumber)

  type Description = String Refined NonEmpty
  object Description extends CustomRefinedTypeOps[Description, String, InvalidDescription](InvalidDescription)
}
