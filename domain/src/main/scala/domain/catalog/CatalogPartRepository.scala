package com.example.ddd
package domain.catalog

import domain.catalog.CatalogPart.PartNumber

trait CatalogPartRepository[F[_], Key] {

  def findByPartNumber[CurrencyCode <: String: ValueOf](
    partNumber: PartNumber
  ): F[Option[CatalogPart[CurrencyCode]]]

}
