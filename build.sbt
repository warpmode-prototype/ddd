import Dependencies._
import Settings._
import sbt.Keys.libraryDependencies

inThisBuild(
  List(
    organization := "com.example.ddd",
    idePackagePrefix.withRank(KeyRanks.Invisible) := Some(organization.value),
  )
)

lazy val root = (project in file("."))
  .aggregate(api, application, domain, infrastructure)
  .dependsOn(api, application, domain, infrastructure)
  .enablePlugins(BuildInfoPlugin, GitVersioning, JavaAppPackaging, DockerPlugin, AshScriptPlugin)
  .settings(commonSettings)
  .settings(buildInfoSettings)
  .settings(
    name := "ddd",
    publishLocal / aggregate := false,
    publish / aggregate := false,
    dockerBaseImage := "adoptopenjdk/openjdk11:alpine-jre",
    dockerExposedPorts ++= List(8080),
    dockerRepository := Some("registry.gitlab.com"),
    dockerUsername := Some("warpmode-prototype"),
    dockerUpdateLatest := true,
    Docker / maintainer := "maintainer",
    libraryDependencies ++= List(
      distageCore,
      distageFramework,
      distageTestkit,
      logbackClassic,
      logstashEncoder,
      pureConfig,
      refinedPureConfig,
      zio,
      zioInteropCats,
    ),
  )

lazy val api = project
  .dependsOn(application, domain)
  .settings(commonSettings)
  .settings(
    libraryDependencies ++= List(
      catsCore,
      catsEffect,
      circeGeneric,
      circeGenericExtras,
      circeLiteral,
      circeRefined,
      distageCore % Optional,
      http4sBlazeClient,
      http4sBlazeServer,
      http4sCirce,
      http4sDsl,
      refined,
      scalaLogging,
    )
  )

lazy val application = project
  .dependsOn(domain)
  .settings(commonSettings)
  .settings(
    libraryDependencies ++= List(
      catsCore,
      distageCore % Optional,
      refined,
    )
  )

lazy val domain = project
  .settings(commonSettings)
  .settings(
    libraryDependencies ++= List(
      catsCore,
      distageCore % Optional,
      refined,
    )
  )

lazy val infrastructure = project
  .dependsOn(api, application, domain)
  .settings(commonSettings)
  .settings(
    libraryDependencies ++= List(
      catsCore,
      catsEffect,
      distageCore % Optional,
      doobieCore,
      doobieHikari,
      doobiePostgres,
      doobieQuill,
      flyway,
      refined,
      scalaLogging,
    )
  )
