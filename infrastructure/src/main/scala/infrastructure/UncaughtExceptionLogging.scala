package com.example.ddd
package infrastructure

import com.typesafe.scalalogging.LazyLogging

trait UncaughtExceptionLogging extends LazyLogging {
  Thread.setDefaultUncaughtExceptionHandler((t, e) => logger.error(s"Uncaught exception in thread: ${t.toString}", e))
}
