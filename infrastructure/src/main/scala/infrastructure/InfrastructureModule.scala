package com.example.ddd
package infrastructure

import application.Transaction
import domain.catalog.CatalogPartRepository
import domain.order.PurchaseOrderRepository
import infrastructure.persistence.DatabaseMigration
import infrastructure.persistence.configuration.DatabaseConfiguration
import infrastructure.persistence.repository.doobie.DoobiePurchaseOrderRepository
import infrastructure.persistence.repository.memory.InMemoryCatalogPartRepository

import cats.effect.{Async, Blocker, Bracket, ContextShift}
import cats.{Applicative, Monad}
import distage.{ModuleDef, TagK}
import doobie.ConnectionIO
import doobie.hikari.HikariTransactor
import doobie.implicits._
import doobie.quill.DoobieContext
import doobie.util.ExecutionContexts
import doobie.util.transactor.Transactor
import io.getquill.SnakeCase

import java.util.UUID

@SuppressWarnings(Array("org.wartremover.warts.NonUnitStatements"))
final class InfrastructureModule[F[_]: Applicative: TagK] extends ModuleDef {
  make[DatabaseConfiguration].from((configuration: InfrastructureConfiguration) => configuration.db)

  make[DatabaseMigration[F]]

  make[DoobieContext.Postgres[SnakeCase.type]].from(new DoobieContext.Postgres(SnakeCase))

  make[CatalogPartRepository[ConnectionIO, UUID]].from(new InMemoryCatalogPartRepository[ConnectionIO])

  make[PurchaseOrderRepository[ConnectionIO, UUID]].from { dc: DoobieContext.Postgres[SnakeCase.type] =>
    new DoobiePurchaseOrderRepository(dc)
  }

  make[Transactor[F]].fromResource {
    (configuration: DatabaseConfiguration, async: Async[F], contextShift: ContextShift[F]) =>
      for {
        ce <- ExecutionContexts.fixedThreadPool[F](configuration.connectionPool.size.value)(async) // await connection here
        be <- Blocker[F](async) // execute JDBC operations here
        xa <- HikariTransactor.newHikariTransactor[F](
          configuration.driver.value,
          configuration.url.value,
          configuration.user,
          configuration.password,
          ce,
          be,
        )(async, contextShift)
      } yield xa
  }

  make[Transaction[ConnectionIO, F]].from(createTransaction _)

  private def createTransaction(
    transactor: Transactor[F],
    bracket: Bracket[F, Throwable],
  ): Transaction[ConnectionIO, F] =
    new Transaction[ConnectionIO, F] {

      override def transact[A](transaction: ConnectionIO[A]): F[A] = transaction.transact[F](transactor)(bracket)

    }

  addImplicit[Monad[ConnectionIO]]
}
