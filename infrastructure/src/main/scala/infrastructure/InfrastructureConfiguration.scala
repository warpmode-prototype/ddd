package com.example.ddd
package infrastructure

import infrastructure.persistence.configuration.DatabaseConfiguration

final case class InfrastructureConfiguration(db: DatabaseConfiguration)
