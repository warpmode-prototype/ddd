package com.example.ddd
package infrastructure.persistence.repository.memory

import domain.Money
import domain.catalog.CatalogPart.{Description, PartNumber}
import domain.catalog.{CatalogPart, CatalogPartRepository}

import cats.Applicative
import cats.implicits._

import java.util.UUID

final class InMemoryCatalogPartRepository[F[_]: Applicative] extends CatalogPartRepository[F, UUID] {

  override def findByPartNumber[CurrencyCode <: String: ValueOf](
    partNumber: PartNumber
  ): F[Option[CatalogPart[CurrencyCode]]] =
    Option
      .when(partNumber.value =!= "unknown")(
        CatalogPart(
          partNumber,
          Description.unsafeFrom(s"Description of part ${partNumber.toString}"),
          Money.unsafeFrom[CurrencyCode](19.99),
        )
      )
      .pure[F]

}
