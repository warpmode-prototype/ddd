package com.example.ddd
package infrastructure.persistence.repository.memory

import domain.catalog.CatalogPart.PartNumber
import domain.order.PurchaseOrder.Id
import domain.order.{PurchaseOrder, PurchaseOrderRepository}
import domain.util._

import cats._
import cats.implicits._

import java.util.UUID
import scala.collection.concurrent.TrieMap

final class InMemoryPurchaseOrderRepository[F[_]: Applicative] extends PurchaseOrderRepository[F, UUID] {
  private val cache = new TrieMap[UUID, PurchaseOrder[UUID, _ <: String]]

  override def nextIdentity: F[Id[UUID]] = Id(UUID.randomUUID()).pure[F]

  override def exists(id: Id[UUID]): F[Boolean] = cache.contains(id.value).pure[F]

  override def findOne(id: Id[UUID]): F[Option[PurchaseOrder[UUID, _ <: String]]] = cache.get(id.value).pure[F]

  override def findByCatalogPart(partNumber: PartNumber): F[Seq[PurchaseOrder[UUID, _ <: String]]] =
    cache.values.toSeq.filter(_.lineItems.toList.exists(_.partNumber === partNumber)).pure[F]

  override def findAll: F[Seq[PurchaseOrder[UUID, _ <: String]]] = cache.values.toSeq.pure[F]

  override def count: F[Long] = cache.size.toLong.pure[F]

  override def save(order: PurchaseOrder[UUID, _ <: String]): F[Unit] =
    cache.put(order.id.value, order).pure[F].void

  override def delete(id: Id[UUID]): F[Unit] = cache.remove(id.value).pure[F].void
}
