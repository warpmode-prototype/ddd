package com.example.ddd
package infrastructure.persistence.repository.doobie.record

import domain.DomainError.DomainErrors
import domain.order.{LineItem, PurchaseOrder}
import domain.{ApprovedLimit, DomainError, Money}

import cats.data.{EitherNec, NonEmptyList}
import cats.implicits._

import java.util.UUID

final case class PurchaseOrderRecord[Key](
  id: PurchaseOrder.Id[Key],
  version: Long,
  currency: String,
  approvedLimitAmount: BigDecimal,
) {

  def toPurchaseOrder[CurrencyCode <: String: ValueOf](
    lineItems: NonEmptyList[LineItem[CurrencyCode]]
  ): EitherNec[DomainError, PurchaseOrder[Key, CurrencyCode]] =
    for {
      approvedLimitValue <- Money.from(approvedLimitAmount).toEitherNec
      approvedLimit      <- ApprovedLimit.from(approvedLimitValue).toEitherNec
      purchaseOrder <- PurchaseOrder[Key, CurrencyCode](
        id,
        version,
        approvedLimit,
        lineItems,
      ).leftWiden[DomainErrors]
    } yield purchaseOrder

}

object PurchaseOrderRecord {

  def toPurchaseOrders(
    records: Seq[(PurchaseOrderRecord[UUID], LineItemRecord[UUID])]
  ): EitherNec[DomainError, Seq[PurchaseOrder[UUID, _ <: String]]] =
    records.groupMap(_._1)(_._2).map { case (p, l) => toPurchaseOrder(p, l) }.toSeq.parSequence

  private def toPurchaseOrder(
    purchaseOrderRecord: PurchaseOrderRecord[UUID],
    lineItemRecords: Seq[LineItemRecord[UUID]],
  ): EitherNec[DomainError, PurchaseOrder[UUID, purchaseOrderRecord.currency.type]] =
    for {
      lineItems     <- LineItemRecord.toLineItems[purchaseOrderRecord.currency.type](lineItemRecords)
      purchaseOrder <- purchaseOrderRecord.toPurchaseOrder(lineItems)
    } yield purchaseOrder

  def fromPurchaseOrder[Key](
    order: PurchaseOrder[Key, _ <: String]
  ): (PurchaseOrderRecord[Key], List[LineItemRecord[Key]]) = {
    import order._

    (
      PurchaseOrderRecord(id, version, approvedLimit.value.currencyCode, approvedLimit.value.amount),
      lineItems.map(LineItemRecord.fromLineItem(order.id)).toList,
    )
  }

}
