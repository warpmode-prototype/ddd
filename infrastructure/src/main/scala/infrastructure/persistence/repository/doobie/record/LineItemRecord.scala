package com.example.ddd
package infrastructure.persistence.repository.doobie.record

import domain.catalog.CatalogPart.{Description, PartNumber}
import domain.order.LineItem.Quantity
import domain.order.{LineItem, PurchaseOrder}
import domain.{DomainError, InvalidPersistedData, Money}

import cats.data.{EitherNec, NonEmptyList}
import cats.implicits._

import java.util.UUID

final case class LineItemRecord[Key](
  purchaseOrderId: PurchaseOrder.Id[Key],
  partNumber: String,
  description: String,
  quantity: Int,
  price: BigDecimal,
) {

  def toLineItem[CurrencyCode <: String: ValueOf]: EitherNec[DomainError, LineItem[CurrencyCode]] =
    (
      PartNumber.from(partNumber).toEitherNec,
      Description.from(description).toEitherNec,
      Quantity.from(quantity).toEitherNec,
      Money.from(price).toEitherNec,
    ).parMapN(LineItem(_, _, _, _))

}

object LineItemRecord {

  def toLineItems[CurrencyCode <: String: ValueOf](
    records: Seq[LineItemRecord[UUID]]
  ): EitherNec[DomainError, NonEmptyList[LineItem[CurrencyCode]]] =
    Either
      .fromOption(NonEmptyList.fromList(records.toList), InvalidPersistedData)
      .toEitherNec
      .flatMap(_.parTraverse(_.toLineItem))

  def fromLineItem[Key](
    purchaseOrderId: PurchaseOrder.Id[Key]
  )(lineItem: LineItem[_ <: String]): LineItemRecord[Key] = {
    import lineItem._

    LineItemRecord(purchaseOrderId, partNumber.value, description.value, quantity.value, price.amount)
  }

}
