package com.example.ddd
package infrastructure.persistence.repository.doobie.quill

import infrastructure.persistence.repository.doobie.record.{LineItemRecord, PurchaseOrderRecord}

import io.getquill.mirrorContextWithQueryProbing.{querySchema, quote}

import java.util.UUID

//noinspection TypeAnnotation
package object schema {

  @SuppressWarnings(Array("org.wartremover.warts.PublicInference"))
  val purchaseOrders = quote {
    querySchema[PurchaseOrderRecord[UUID]]("purchase_order")
  }

  @SuppressWarnings(Array("org.wartremover.warts.PublicInference"))
  val lineItems = quote {
    querySchema[LineItemRecord[UUID]]("line_item")
  }

}
