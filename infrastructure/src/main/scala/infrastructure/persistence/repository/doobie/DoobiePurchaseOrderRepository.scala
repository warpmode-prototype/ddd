package com.example.ddd
package infrastructure.persistence.repository.doobie

import domain.catalog.CatalogPart.PartNumber
import domain.order.{PurchaseOrder, PurchaseOrderRepository}
import infrastructure.persistence.repository.doobie.DoobiePurchaseOrderRepository.Mapper
import infrastructure.persistence.repository.doobie.quill.schema.{lineItems, purchaseOrders}
import infrastructure.persistence.repository.doobie.record.{LineItemRecord, PurchaseOrderRecord}

import cats.implicits._
import doobie.ConnectionIO
import doobie.quill.DoobieContext
import io.getquill.{idiom => _, _}

import java.util.UUID

@SuppressWarnings(
  Array("org.wartremover.warts.JavaSerializable", "org.wartremover.warts.Product", "org.wartremover.warts.Serializable")
)
final class DoobiePurchaseOrderRepository(dc: DoobieContext.Postgres[SnakeCase.type])
    extends PurchaseOrderRepository[ConnectionIO, UUID] {

  import dc._

  override def nextIdentity: ConnectionIO[PurchaseOrder.Id[UUID]] =
    PurchaseOrder.Id(UUID.randomUUID).pure[ConnectionIO]

  override def exists(id: PurchaseOrder.Id[UUID]): ConnectionIO[Boolean] =
    run(purchaseOrders.map(_.id).filter(_ == lift(id)).nonEmpty)

  override def findOne(id: PurchaseOrder.Id[UUID]): ConnectionIO[Option[PurchaseOrder[UUID, _ <: String]]] = {
    val ids = quote {
      purchaseOrders.map(_.id).filter(_ == lift(id))
    }

    run(query(ids)).mapResult.map(_.headOption)
  }

  override def findByCatalogPart(partNumber: PartNumber): ConnectionIO[Seq[PurchaseOrder[UUID, _ <: String]]] = {
    val ids = quote {
      lineItems.filter(_.partNumber == lift(partNumber.value)).map(_.purchaseOrderId)
    }

    run(query(ids)).mapResult
  }

  override def findAll: ConnectionIO[Seq[PurchaseOrder[UUID, _ <: String]]] = {
    val ids = quote {
      purchaseOrders.map(_.id)
    }

    run(query(ids)).mapResult
  }

  override def count: ConnectionIO[Long] = run(purchaseOrders.size)

  override def save(order: PurchaseOrder[UUID, _ <: String]): ConnectionIO[Unit] = {
    val (orderRecord, lineItemRecords) = PurchaseOrderRecord.fromPurchaseOrder(order)

    // TODO: Consider version for optimistic concurrency control

    val upsertPurchaseOrder = quote {
      purchaseOrders
        .insert(lift(orderRecord))
        .onConflictUpdate(_.id)(
          _.version             -> _.version,
          _.currency            -> _.currency,
          _.approvedLimitAmount -> _.approvedLimitAmount,
        )
    }

    val deleteExistingLineItems = quote {
      lineItems.filter(_.purchaseOrderId == lift(order.id)).delete
    }

    val insertNewLineItems = quote {
      liftQuery(lineItemRecords).foreach(lineItems.insert(_))
    }

    run(upsertPurchaseOrder) *> run(deleteExistingLineItems) *> run(insertNewLineItems).void
  }

  override def delete(id: PurchaseOrder.Id[UUID]): ConnectionIO[Unit] =
    run(purchaseOrders.filter(_.id == lift(id)).delete).map(_ => ())

  private val query =
    quote { ids: Query[PurchaseOrder.Id[UUID]] =>
      for {
        po <- purchaseOrders.filter(po => ids.contains(po.id))
        li <- lineItems if po.id == li.purchaseOrderId
      } yield (po, li)
    }

}

object DoobiePurchaseOrderRepository {

  implicit final class Mapper(val result: ConnectionIO[List[(PurchaseOrderRecord[UUID], LineItemRecord[UUID])]])
      extends AnyVal {

    def mapResult[A]: ConnectionIO[Seq[PurchaseOrder[UUID, _ <: String]]] =
      result.map(records => PurchaseOrderRecord.toPurchaseOrders(records).valueOr(failInvalidData))

  }

}
