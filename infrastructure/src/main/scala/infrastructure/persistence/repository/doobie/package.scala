package com.example.ddd
package infrastructure.persistence.repository

import domain.DomainError

import cats.data.NonEmptyChain
import cats.implicits._

package object doobie {

  @SuppressWarnings(Array("org.wartremover.warts.Throw"))
  def failInvalidData(errors: NonEmptyChain[DomainError]): Nothing = throw new IllegalStateException(
    s"Invalid persisted data: \n\n ${errors.toList.mkString("\n")}"
  )

}
