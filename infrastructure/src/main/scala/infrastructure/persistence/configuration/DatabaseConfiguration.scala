package com.example.ddd
package infrastructure.persistence.configuration

import infrastructure.persistence.configuration.DatabaseConfiguration.{ConnectionUrl, Driver, Password, User}

import eu.timepit.refined.api.Refined
import eu.timepit.refined.string.{MatchesRegex, Uri}

final case class DatabaseConfiguration(
  url: ConnectionUrl,
  driver: Driver,
  user: User,
  password: Password,
  connectionPool: ConnectionPoolConfiguration,
)

object DatabaseConfiguration {
  type ConnectionUrl = String Refined Uri
  type Driver        = String Refined MatchesRegex["""([a-zA-Z_$][a-zA-Z\d_$]*\.)*[a-zA-Z_$][a-zA-Z\d_$]*"""]
  type User          = String
  type Password      = String
}
