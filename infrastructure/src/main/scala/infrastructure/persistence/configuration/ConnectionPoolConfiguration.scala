package com.example.ddd
package infrastructure.persistence.configuration

import infrastructure.persistence.configuration.ConnectionPoolConfiguration.PoolSize

import eu.timepit.refined.api.Refined
import eu.timepit.refined.numeric.Interval

final case class ConnectionPoolConfiguration(size: PoolSize)

object ConnectionPoolConfiguration {

  /** https://github.com/brettwooldridge/HikariCP/wiki/About-Pool-Sizing */
  type PoolSize = Int Refined Interval.Open[1, 100]
}
