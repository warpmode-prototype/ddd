package com.example.ddd
package infrastructure.persistence

import infrastructure.persistence.configuration.DatabaseConfiguration

import cats.effect.Sync
import org.flywaydb.core.Flyway
import cats.implicits._

final class DatabaseMigration[F[_]: Sync](databaseConfiguration: DatabaseConfiguration) {

  def run: F[Unit] =
    Sync[F].delay {
      import databaseConfiguration._
      val flyway = Flyway.configure().dataSource(url.value, user, password).load
      flyway.migrate()
    }.void

}
