# DDD

A Scala prototype for a restful microservice with the following ideas and goals:

- Collect boilerplate and best practices from former projects in a single repo
- Marry domain-driven design patterns and principles with pure functional programming
- Try out some *wild* design ideas to find out how well they work in practice
- Play with the typelevel stack

*Still a work in progress!*
