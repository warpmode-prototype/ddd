package com.example.ddd
package api.http

import api.http.HttpServerConfiguration.Port

import eu.timepit.refined.api.Refined
import eu.timepit.refined.numeric.Greater

final case class HttpServerConfiguration(port: Port)

object HttpServerConfiguration {
  type Port = Int Refined Greater[1024]
}
