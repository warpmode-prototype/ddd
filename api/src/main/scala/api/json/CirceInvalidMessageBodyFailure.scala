package com.example.ddd
package api.json

import cats.implicits._
import io.circe.DecodingFailure
import org.http4s.circe.DecodingFailures
import org.http4s.{EntityEncoder, HttpVersion, MessageBodyFailure, Response, Status}

@SuppressWarnings(Array("IncorrectlyNamedExceptions", "org.wartremover.warts.DefaultArguments"))
final case class CirceInvalidMessageBodyFailure(details: String, cause: Option[Throwable] = None)
    extends MessageBodyFailure {

  def message: String =
    s"Invalid message body: $details"

  def toHttpResponse[F[_]](httpVersion: HttpVersion): Response[F] =
    Response(Status.UnprocessableEntity, httpVersion).withEntity(getDetailedMessage)(EntityEncoder.stringEncoder[F])

  private def getDetailedMessage: String =
    cause match {
      case Some(decodingFailure: DecodingFailure) => s"The request body was invalid: \n\n ${decodingFailure.show}"
      case Some(decodingFailures: DecodingFailures) =>
        s"The request body was invalid: \n\n ${decodingFailures.failures.map(_.show).toList.mkString("\n")}"
      case _ => s"The request body was invalid."
    }

}
