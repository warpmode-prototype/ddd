package com.example.ddd
package api

import domain.{ApprovedLimit, Money}
import domain.catalog.CatalogPart.{Description, PartNumber}
import domain.order.LineItem.Quantity
import domain.order.PurchaseOrder.Id
import domain.order.{LineItem, OrderQuantity, PurchaseOrder}

import cats.data.NonEmptyList
import cats.implicits._
import io.circe.generic.extras.semiauto.{deriveUnwrappedDecoder, deriveUnwrappedEncoder}
import io.circe.generic.semiauto._
import io.circe.refined._
import io.circe.{Codec, Decoder, Encoder, Printer}
import org.http4s.circe.{CirceInstances, DecodingFailures}

import java.util.UUID

package object json {
  implicit val printer: Printer = Printer.noSpaces.copy(dropNullValues = true)

  object key {
    implicit val uuidDecoder: Decoder[UUID] = Decoder.decodeUUID
    implicit val uuidEncoder: Encoder[UUID] = Encoder.encodeUUID
    implicit val uuidCodec: Codec[UUID]     = Codec.from(uuidDecoder, uuidEncoder)

    implicit val intDecoder: Decoder[Int] = Decoder.decodeInt
    implicit val intEncoder: Encoder[Int] = Encoder.encodeInt
    implicit val intCodec: Codec[Int]     = Codec.from(intDecoder, intEncoder)
  }

  val circe: CirceInstances = CirceInstances.builder.withJsonDecodeError { (json, failures) =>
    CirceInvalidMessageBodyFailure(
      s"Could not decode JSON: ${json.toString}",
      if (failures.tail.isEmpty) Some(failures.head) else Some(DecodingFailures(failures)),
    )
  }.build

  // Money

  implicit def moneyEncoder[CurrencyCode <: String]: Encoder[Money[CurrencyCode]] =
    moneyEncoderAnyCurrency.contramap(identity)

  implicit val moneyEncoderAnyCurrency: Encoder[Money[_ <: String]] =
    Encoder.forProduct2("amount", "currency")(money => (money.amount, money.currency.getCurrencyCode))

  implicit def moneyDecoder[CurrencyCode <: String: ValueOf]: Decoder[Money[CurrencyCode]] =
    Decoder
      .forProduct2("amount", "currency")((amount: BigDecimal, _: CurrencyCode) => Money.from[CurrencyCode](amount))
      .emap(_.leftMap(_.toString))

  implicit val moneyDecoderAnyCurrency: Decoder[Money[_ <: String]] =
    Decoder
      .forProduct2("amount", "currency")((amount: BigDecimal, currency: String) => Money.from[currency.type](amount))
      .emap(_.leftMap(_.toString))

  // LineItem

  implicit def lineItemEncoder[CurrencyCode <: String: ValueOf]: Encoder[LineItem[CurrencyCode]] = deriveEncoder

  implicit val lineItemEncoderAnyCurrency: Encoder[LineItem[_ <: String]] =
    Encoder.forProduct3("description", "quantity", "price")(lineItem =>
      (lineItem.description, lineItem.quantity, lineItem.price)
    )

  implicit def lineItemDecoder[CurrencyCode <: String: ValueOf]: Decoder[LineItem[CurrencyCode]] = deriveDecoder

  @SuppressWarnings(Array("AsInstanceOf", "org.wartremover.warts.AsInstanceOf"))
  implicit val lineItemDecoderAnyCurrency: Decoder[LineItem[_ <: String]] =
    Decoder.forProduct4("partNumber", "description", "quantity", "price") {
      (partNumber: PartNumber, description: Description, quantity: Quantity, price: Money[_ <: String]) =>
        LineItem[price.currencyCode.type](
          partNumber,
          description,
          quantity,
          price.asInstanceOf[Money[price.currencyCode.type]],
        )
    }

  // ApprovedLimit

  implicit val approvedLimitEncoderAnyCurrency: Encoder[ApprovedLimit[_ <: String]] =
    moneyEncoderAnyCurrency.contramap(_.value)

  implicit val approvedLimitDecoderAnyCurrency: Decoder[ApprovedLimit[_ <: String]] =
    moneyDecoderAnyCurrency.emap(ApprovedLimit.from(_).leftMap(_.toString))

  // PurchaseOrder

  implicit def purchaseOrderIdDecoder[Key: Decoder]: Decoder[Id[Key]] = deriveUnwrappedDecoder
  implicit def purchaseOrderIdEncoder[Key: Encoder]: Encoder[Id[Key]] = deriveUnwrappedEncoder

  implicit def purchaseOrderEncoder[Key: Encoder, CurrencyCode <: String]: Encoder[PurchaseOrder[Key, CurrencyCode]] =
    purchaseOrderEncoderAnyCurrency[Key].contramap(identity)

  implicit def purchaseOrderEncoderAnyCurrency[Key: Encoder]: Encoder[PurchaseOrder[Key, _ <: String]] =
    Encoder.forProduct4("id", "version", "approvedLimit", "lineItems") { po =>
      (po.id, po.version, po.approvedLimit, po.lineItems)
    }

  implicit def purchaseOrderDecoder[Key: Decoder, CurrencyCode <: String: ValueOf]
    : Decoder[PurchaseOrder[Key, CurrencyCode]] =
    Decoder
      .forProduct4("id", "version", "approvedLimit", "lineItems") {
        (
          id: Id[Key],
          version: Long,
          approvedLimit: ApprovedLimit[CurrencyCode],
          lineItems: NonEmptyList[LineItem[CurrencyCode]],
        ) => PurchaseOrder(id, version, approvedLimit, lineItems)
      }
      .emap(_.leftMap(_.toString))

  @SuppressWarnings(Array("AsInstanceOf", "org.wartremover.warts.AsInstanceOf"))
  implicit def purchaseOrderDecoderAnyCurrency[Key: Decoder]: Decoder[PurchaseOrder[Key, _ <: String]] =
    Decoder
      .instance { cursor =>
        for {
          id            <- cursor.downField("id").as[Id[Key]]
          version       <- cursor.downField("version").as[Long]
          approvedLimit <- cursor.downField("approvedLimit").as[ApprovedLimit[_ <: String]]
          lineItems     <- cursor.downField("lineItems").as[NonEmptyList[LineItem[approvedLimit.value.currencyCode.type]]]
        } yield {
          type CurrencyCode = approvedLimit.value.currencyCode.type

          PurchaseOrder[Key, CurrencyCode](
            id,
            version,
            approvedLimit.asInstanceOf[ApprovedLimit[CurrencyCode]],
            lineItems,
          )
        }
      }
      .emap(_.leftMap(_.toString))

  implicit val orderQuantityDecoder: Decoder[OrderQuantity] = deriveDecoder
}
