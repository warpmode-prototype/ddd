package com.example.ddd
package api

import api.control.{HealthController, PurchaseOrderController, VersionController}
import api.http.HttpServerConfiguration

import cats.Monad
import cats.data.Kleisli
import cats.effect.{ConcurrentEffect, Sync, Timer}
import cats.implicits._
import com.typesafe.scalalogging.StrictLogging
import distage.{ModuleDef, Tag, TagK}
import eu.timepit.refined.auto._
import io.circe.Codec
import org.http4s._
import org.http4s.implicits._
import org.http4s.server.Server
import org.http4s.server.blaze.BlazeServerBuilder

import scala.concurrent.ExecutionContext.Implicits.global

@SuppressWarnings(Array("org.wartremover.warts.NonUnitStatements"))
final class ApiModule[F[_]: Monad: TagK, Key: Tag: Codec, TX[_]: TagK](version: String)
    extends ModuleDef
    with StrictLogging {

  make[HttpServerConfiguration].from((configuration: ApiConfiguration) => configuration.httpServer)

  make[HealthController[F]]
  many[HttpRoutes[F]].add { c: HealthController[F] => c.routes }

  make[VersionController[F]].from((s: Sync[F]) => new VersionController[F](version)(s))
  many[HttpRoutes[F]].add { c: VersionController[F] => c.routes }

  make[PurchaseOrderController[F, Key, TX]]
  many[HttpRoutes[F]].add { c: PurchaseOrderController[F, Key, TX] => c.routes }

  make[Server[F]].fromResource {
    (
      httpServerConfiguration: HttpServerConfiguration,
      routes: Set[HttpRoutes[F]],
      concurrentEffect: ConcurrentEffect[F],
      timer: Timer[F],
    ) =>
      def logRequest(service: HttpRoutes[F]): HttpRoutes[F] =
        Kleisli { (req: Request[F]) =>
          logger.info(s"Handling HTTP request: ${req.uri.toString}")

          service(req)
        }

      // create a top-level router by combining all the routes
      val router: HttpApp[F] = logRequest(routes.toList.foldK).orNotFound

      // return a Resource value that will setup an http4s server
      BlazeServerBuilder[F](global)(concurrentEffect, timer)
        .bindHttp(httpServerConfiguration.port, "0.0.0.0")
        .withHttpApp(router)
        .resource
  }

  addImplicit[Codec[Key]]
}
