package com.example.ddd
package api

import api.http.HttpServerConfiguration

final case class ApiConfiguration(httpServer: HttpServerConfiguration)
