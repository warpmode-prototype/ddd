package com.example.ddd
package api.control

import cats.effect.Sync
import org.http4s.HttpRoutes
import org.http4s.dsl.Http4sDsl

final class HealthController[F[_]: Sync] extends Http4sDsl[F] {

  def routes: HttpRoutes[F] =
    HttpRoutes.of[F] {
      case GET -> Root / "health" / "liveness"  => Ok("live")
      case GET -> Root / "health" / "readiness" => Ok("ready") // TODO: Check dependencies
    }

}
