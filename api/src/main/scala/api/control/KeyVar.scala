package com.example.ddd
package api.control

import io.circe.syntax._
import io.circe.{Codec, Decoder}

object KeyVar {

  def unapply[Key: Codec](s: String): Option[Key] =
    if (s.nonEmpty)
      Decoder[Key].decodeJson(s.asJson).toOption
    else
      None

}
