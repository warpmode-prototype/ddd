package com.example.ddd
package api.control

import cats.effect.Sync
import org.http4s.HttpRoutes
import org.http4s.dsl.Http4sDsl

final class VersionController[F[_]: Sync](version: String) extends Http4sDsl[F] {

  def routes: HttpRoutes[F] =
    HttpRoutes.of[F] { case GET -> Root / "version" => Ok(version) }

}
