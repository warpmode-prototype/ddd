package com.example.ddd
package api.control

import api.control.PurchaseOrderController.OptionalPartNumberQueryParamMatcher
import api.json._
import application.service.PurchaseOrderService
import domain.DomainError.DomainErrors
import domain.catalog.CatalogPart.PartNumber
import domain.order.{OrderQuantity, PurchaseOrder}

import cats.data.{EitherT, NonEmptyList}
import cats.effect.Sync
import cats.implicits._
import io.circe.Codec
import io.circe.syntax._
import org.http4s.circe._
import org.http4s.dsl.Http4sDsl
import org.http4s.dsl.io.OptionalValidatingQueryParamDecoderMatcher
import org.http4s.{EntityDecoder, HttpRoutes, ParseFailure, QueryParamDecoder}

final class PurchaseOrderController[F[_] : Sync, Key: Codec, TX[_]](
  purchaseOrderService: PurchaseOrderService[F, Key, TX])
  extends Http4sDsl[F] {

  private implicit val orderQuantityEntityDecoder: EntityDecoder[F, NonEmptyList[OrderQuantity]] =
    api.json.circe.accumulatingJsonOf[F, NonEmptyList[OrderQuantity]]

  def routes: HttpRoutes[F] =
    HttpRoutes.of[F] {
      case GET -> Root / "orders" :? OptionalPartNumberQueryParamMatcher(maybePartNumber) =>
        maybePartNumber match {
          case Some(validatedPartNumber) =>
            validatedPartNumber.fold(
              errors => BadRequest(errors.toList.mkString(",")), { partNumber =>
                for {
                  orders <- purchaseOrderService.findByCatalogPart(partNumber)
                  response <- Ok(orders.asJson)
                } yield response
              })
          case None =>
            for {
              orders <- purchaseOrderService.findAllOrders
              response <- Ok(orders.asJson)
            } yield response
        }
      case GET -> Root / "orders" / KeyVar(orderId) =>
        for {
          maybeOrder <- purchaseOrderService.findOrder(PurchaseOrder.Id(orderId))
          response   <- maybeOrder.fold(NotFound())(order => Ok(order.asJson))
        } yield response
      case request @ POST -> Root / "orders" =>
        (for {
          quantities <- EitherT.right[DomainErrors](request.as[NonEmptyList[OrderQuantity]])
          order      <- EitherT(purchaseOrderService.createOrder(quantities))
        } yield order).value.flatMap {
          case Right(_)     => Created()
          case Left(errors) => BadRequest(errors.toList.mkString("\n"))
        }
      case DELETE -> Root / "orders" / KeyVar(orderId) =>
        for {
          _        <- purchaseOrderService.deleteOrder(PurchaseOrder.Id(orderId))
          response <- NoContent()
        } yield response
    }

}

object PurchaseOrderController {

  private implicit val partNumberQueryParamDecoder: QueryParamDecoder[com.example.ddd.domain.catalog.CatalogPart.PartNumber] =
    QueryParamDecoder[String].emap(s => PartNumber.from(s).leftMap(e => ParseFailure(e.toString, "")))

  private object OptionalPartNumberQueryParamMatcher extends OptionalValidatingQueryParamDecoderMatcher[PartNumber](
    "partno")

}
