import sbt._

object Dependencies {

  object Versions {
    val cats            = "2.5.0"
    val catsEffect      = "2.4.1"
    val circe           = "0.13.0"
    val distage         = "1.0.5"
    val doobie          = "0.12.1"
    val flyway          = "7.7.2"
    val http4s          = "0.21.21"
    val logback         = "1.2.3"
    val logstashEncoder = "6.6"
    val newType         = "0.4.4"
    val pureConfig      = "0.14.1"
    val refined         = "0.9.23"
    val scalaLogging    = "3.9.3"
    val zio             = "1.0.5"
    val zioInteropCats  = "2.2.0.1"
  }

  // Compile
  val catsCore           = "org.typelevel"              %% "cats-core"            % Versions.cats
  val catsEffect         = "org.typelevel"              %% "cats-effect"          % Versions.catsEffect
  val circeGeneric       = "io.circe"                   %% "circe-generic"        % Versions.circe
  val circeGenericExtras = "io.circe"                   %% "circe-generic-extras" % Versions.circe
  val circeLiteral       = "io.circe"                   %% "circe-literal"        % Versions.circe
  val circeParser        = "io.circe"                   %% "circe-parser"         % Versions.circe
  val circeRefined       = "io.circe"                   %% "circe-refined"        % Versions.circe
  val distageCore        = "io.7mind.izumi"             %% "distage-core"         % Versions.distage
  val distageFramework   = "io.7mind.izumi"             %% "distage-framework"    % Versions.distage
  val doobieCore         = "org.tpolecat"               %% "doobie-core"          % Versions.doobie
  val doobieHikari       = "org.tpolecat"               %% "doobie-hikari"        % Versions.doobie
  val doobiePostgres     = "org.tpolecat"               %% "doobie-postgres"      % Versions.doobie
  val doobieQuill        = "org.tpolecat"               %% "doobie-quill"         % Versions.doobie
  val flyway             = "org.flywaydb"                % "flyway-core"          % Versions.flyway
  val http4sBlazeClient  = "org.http4s"                 %% "http4s-blaze-client"  % Versions.http4s
  val http4sBlazeServer  = "org.http4s"                 %% "http4s-blaze-server"  % Versions.http4s
  val http4sCirce        = "org.http4s"                 %% "http4s-circe"         % Versions.http4s
  val http4sDsl          = "org.http4s"                 %% "http4s-dsl"           % Versions.http4s
  val scalaLogging       = "com.typesafe.scala-logging" %% "scala-logging"        % Versions.scalaLogging
  val newType            = "io.estatico"                %% "newtype"              % Versions.newType
  val pureConfig         = "com.github.pureconfig"      %% "pureconfig"           % Versions.pureConfig
  val refined            = "eu.timepit"                 %% "refined"              % Versions.refined
  val refinedPureConfig  = "eu.timepit"                 %% "refined-pureconfig"   % Versions.refined
  val zio                = "dev.zio"                    %% "zio"                  % Versions.zio
  val zioInteropCats     = "dev.zio"                    %% "zio-interop-cats"     % Versions.zioInteropCats

  // Runtime
  val logbackClassic  = "ch.qos.logback"       % "logback-classic"          % Versions.logback         % Runtime
  val logstashEncoder = "net.logstash.logback" % "logstash-logback-encoder" % Versions.logstashEncoder % Runtime

  // Test
  val distageTestkit  = "io.7mind.izumi" %% "distage-testkit-scalatest" % Versions.distage % Test
  val doobieScalaTest = "org.tpolecat"   %% "doobie-scalatest"          % Versions.doobie  % Test
}
