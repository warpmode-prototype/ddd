import sbt.Keys._
import sbt._
import sbtbuildinfo.BuildInfoPlugin.autoImport.{
  buildInfoKeys,
  buildInfoOptions,
  buildInfoPackage,
  BuildInfoKey,
  BuildInfoOption,
}

import java.time.Instant

object Settings {
  // Need to be assigned per project as some settings depend on project-specific settings (e.g. sourceManaged)
  lazy val commonSettings = coreSettings ::: scapegoatSettings ::: wartRemoverSettings

  lazy val coreSettings = List(
    scalaVersion := "2.13.5",
    Compile / scalacOptions ++=
      (if (insideCI.value) "-Wconf:any:error" else "-Wconf:any:warning") :: commonScalacOptions,
    Test / scalacOptions := (Compile / scalacOptions).value,
    Compile / console / scalacOptions --= filteredConsoleScalacOptions,
    addCompilerPlugin(("org.typelevel" %% "kind-projector" % "0.11.3").cross(CrossVersion.full)),
    fork := true,
    turbo := true,
    usePipelining := true,
  )

  lazy val commonScalacOptions = List(
    // format: off
    "-deprecation", // Emit warning and location for usages of deprecated APIs.
    "-explaintypes", // Explain type errors in more detail.
    "-feature", // Emit warning and location for usages of features that should be imported explicitly.
    "-language:higherKinds", // Allow higher-kinded types TODO: redundant as of Scala 2.13.3 but still required by IntelliJ
    "-unchecked", // Enable additional warnings where generated code depends on assumptions.
    "-Wdead-code", // Warn when dead code is identified.
    "-Wextra-implicit", // Warn when more than one implicit parameter section is defined.
    "-Wnumeric-widen", // Warn when numerics are widened.
    "-Wunused:explicits", // Warn if an explicit parameter is unused.
    "-Wunused:implicits", // Warn if an implicit parameter is unused.
    "-Wunused:imports", // Warn if an import selector is not referenced.
    "-Wunused:locals", // Warn if a local definition is unused.
    /* "-Wunused:params", // Warn if a value parameter is unused. TODO: disabled temporarily due to wrong unused
     * context bounds warnings */
    "-Wunused:patvars", // Warn if a variable bound in a pattern is unused.
    "-Wunused:privates", // Warn if a private member is unused.
    "-Wvalue-discard", // Warn when non-Unit expression results are unused.
    "-Xcheckinit", // Wrap field accessors to throw an exception on uninitialized access.
    "-Xlint:adapted-args", // Warn if an argument list is modified to match the receiver.
    "-Xlint:constant", // Evaluation of a constant arithmetic expression results in an error.
    "-Xlint:delayedinit-select", // Selecting member of DelayedInit.
    "-Xlint:doc-detached", // A Scaladoc comment appears to be detached from its element.
    "-Xlint:implicit-recursion", // Warn when an implicit resolves to an enclosing self-definition
    "-Xlint:inaccessible", // Warn about inaccessible types in method signatures.
    "-Xlint:infer-any", // Warn when a type argument is inferred to be `Any`.
    "-Xlint:missing-interpolator", // A string literal appears to be missing an interpolator id.
    "-Xlint:nullary-unit", // Warn when nullary methods return Unit.
    "-Xlint:option-implicit", // Option.apply used implicit view.
    "-Xlint:package-object-classes", // Class or object defined in package object.
    "-Xlint:poly-implicit-overload", // Parameterized overloaded implicit methods are not visible as view bounds.
    "-Xlint:private-shadow", // A private field (or class parameter) shadows a superclass field.
    "-Xlint:stars-align", // Pattern sequence wildcard must align with sequence component.
    "-Xlint:type-parameter-shadow", // A local type parameter shadows a type already in scope.
    "-Ybackend-parallelism",
    "8", // Enable parallelization — change to desired number!
    "-Ycache-plugin-class-loader:last-modified", // Enables caching of classloaders for compiler plugins
    "-Ycache-macro-class-loader:last-modified", // and macro definitions. This can lead to performance improvements.
    "-Ymacro-annotations", // Enable support for macro annotations, formerly in macro paradise.
    // format: on
  )

  lazy val filteredConsoleScalacOptions = List(
    "-Werror",
    "-Wdead-code",
    "-Wunused:imports",
    "-Ywarn-unused:imports",
    "-Ywarn-unused-import",
    "-Ywarn-dead-code",
    "-Xfatal-warnings",
  )

  lazy val wartRemoverSettings = List(
    addCompilerPlugin(("org.wartremover" %% "wartremover" % "2.4.13").cross(CrossVersion.full)),
    Compile / scalacOptions ++= {
      if (insideCI.value) wartRemoverWarts("traverser")
      else wartRemoverWarts("only-warn-traverser")
    },
    Compile / scalacOptions += s"-P:wartremover:excluded:${(Compile / sourceManaged).value}",
    Compile / console / scalacOptions ~= (_.filterNot(_.contains("wartremover"))),
  )

  def wartRemoverWarts(traverser: String): List[String] = List(
    s"-P:wartremover:$traverser:org.wartremover.warts.AnyVal",
    s"-P:wartremover:$traverser:org.wartremover.warts.AsInstanceOf",
    s"-P:wartremover:$traverser:org.wartremover.warts.DefaultArguments",
    s"-P:wartremover:$traverser:org.wartremover.warts.EitherProjectionPartial",
    s"-P:wartremover:$traverser:org.wartremover.warts.Enumeration",
    s"-P:wartremover:$traverser:org.wartremover.warts.Equals",
    s"-P:wartremover:$traverser:org.wartremover.warts.ExplicitImplicitTypes",
    s"-P:wartremover:$traverser:org.wartremover.warts.FinalCaseClass",
    s"-P:wartremover:$traverser:org.wartremover.warts.FinalVal",
    s"-P:wartremover:$traverser:org.wartremover.warts.ImplicitConversion",
    s"-P:wartremover:$traverser:org.wartremover.warts.IsInstanceOf",
    s"-P:wartremover:$traverser:org.wartremover.warts.JavaConversions",
    s"-P:wartremover:$traverser:org.wartremover.warts.JavaSerializable",
    /* s"-P:wartremover:$traverser:org.wartremover.warts.MutableDataStructures", // Causes compilation issues since
     * Scala 2.13.4 */
    s"-P:wartremover:$traverser:org.wartremover.warts.NonUnitStatements",
    s"-P:wartremover:$traverser:org.wartremover.warts.Null",
    s"-P:wartremover:$traverser:org.wartremover.warts.Option2Iterable",
    s"-P:wartremover:$traverser:org.wartremover.warts.OptionPartial",
    s"-P:wartremover:$traverser:org.wartremover.warts.Product",
    s"-P:wartremover:$traverser:org.wartremover.warts.PublicInference",
    s"-P:wartremover:$traverser:org.wartremover.warts.Recursion",
    s"-P:wartremover:$traverser:org.wartremover.warts.Return",
    s"-P:wartremover:$traverser:org.wartremover.warts.Serializable",
    s"-P:wartremover:$traverser:org.wartremover.warts.StringPlusAny",
    s"-P:wartremover:$traverser:org.wartremover.warts.Throw",
    s"-P:wartremover:$traverser:org.wartremover.warts.TraversableOps",
    s"-P:wartremover:$traverser:org.wartremover.warts.TryPartial",
    s"-P:wartremover:$traverser:org.wartremover.warts.Var",
    s"-P:wartremover:$traverser:org.wartremover.warts.While",
  )

  lazy val scapegoatSettings = List(
    addCompilerPlugin(("com.sksamuel.scapegoat" %% "scalac-scapegoat-plugin" % "1.4.8").cross(CrossVersion.full)),
    Compile / scalacOptions ++= List(
      "-P:scapegoat:dataDir:./target/scapegoat",
      "-P:scapegoat:disabledInspections:BoundedByFinalType",
    ),
    Compile / console / scalacOptions ~= (_.filterNot(_.contains("scapegoat"))),
  )

  lazy val buildInfoSettings = List(
    buildInfoPackage := organization.value,
    buildInfoKeys := Seq[BuildInfoKey](
      name,
      BuildInfoKey.map(version) { case (k, v) => k -> v.replace("-SNAPSHOT", "") },
      BuildInfoKey.action("buildTime")(Instant.now()),
    ),
    buildInfoOptions += BuildInfoOption.ToJson,
  )

}
