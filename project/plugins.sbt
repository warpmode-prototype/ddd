addSbtPlugin("com.eed3si9n"     % "sbt-buildinfo"       % "0.10.0")
addSbtPlugin("com.typesafe.sbt" % "sbt-git"             % "1.0.0")
addSbtPlugin("org.jetbrains"    % "sbt-ide-settings"    % "1.1.0")
addSbtPlugin("com.typesafe.sbt" % "sbt-native-packager" % "1.8.0")
addSbtPlugin("org.scoverage"    % "sbt-scoverage"       % "1.6.1")

// Automagically configure scalac options according to the project Scala version
// addSbtPlugin("io.github.davidgregory084" % "sbt-tpolecat" % "0.1.13")
