package com.example.ddd
package application

import application.service.PurchaseOrderService

import distage.{ModuleDef, Tag, TagK}

@SuppressWarnings(Array("org.wartremover.warts.NonUnitStatements"))
final class ApplicationModule[F[_]: TagK, Key: Tag, TX[_]: TagK] extends ModuleDef {
  make[PurchaseOrderService[F, Key, TX]]
}
