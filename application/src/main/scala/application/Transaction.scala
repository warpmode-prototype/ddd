package com.example.ddd
package application

trait Transaction[TX[_], F[_]] {
  def transact[A](transaction: TX[A]): F[A]
}

object Transaction {
  def apply[TX[_] : Transaction[*[_], F], F[_]]: Transaction[TX, F] = implicitly[Transaction[TX, F]]

  object syntax {

    final implicit class TransactionOps[TX[_], A](val transaction: TX[A]) extends AnyVal {
      def transact[F[_]](implicit T: Transaction[TX, F]): F[A] = T.transact(transaction)
    }

  }

}
