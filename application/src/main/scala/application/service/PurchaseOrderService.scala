package com.example.ddd
package application.service

import application.Transaction
import application.Transaction.syntax._
import domain.DomainError
import domain.DomainError.DomainErrors
import domain.catalog.CatalogPart.PartNumber
import domain.order.{OrderQuantity, PurchaseOrder, PurchaseOrderFactory, PurchaseOrderRepository}

import cats.Monad
import cats.data.{EitherNec, EitherT, NonEmptyList}

final class PurchaseOrderService[F[_], Key, TX[_]: Monad: Transaction[*[_], F]](
  purchaseOrderFactory: PurchaseOrderFactory[TX, Key],
  purchaseOrderRepository: PurchaseOrderRepository[TX, Key],
) {

  def findAllOrders: F[Seq[PurchaseOrder[Key, _ <: String]]] = purchaseOrderRepository.findAll.transact

  def findByCatalogPart(partNumber: PartNumber): F[Seq[PurchaseOrder[Key, _ <: String]]] =
    purchaseOrderRepository.findByCatalogPart(partNumber).transact

  def findOrder(id: PurchaseOrder.Id[Key]): F[Option[PurchaseOrder[Key, _ <: String]]] =
    purchaseOrderRepository.findOne(id).transact

  def createOrder(
    orderQuantities: NonEmptyList[OrderQuantity]
  ): F[EitherNec[DomainError, PurchaseOrder[Key, "EUR"]]] =
    (for {
      order <- EitherT(purchaseOrderFactory.create["EUR"](orderQuantities))
      _     <- EitherT.right[DomainErrors](purchaseOrderRepository.save(order))
    } yield order).value.transact

  def deleteOrder(id: PurchaseOrder.Id[Key]): F[Unit] = purchaseOrderRepository.delete(id).transact

}
