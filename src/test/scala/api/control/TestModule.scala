package com.example.ddd
package api.control

import api.json.key._
import distage.plugins.PluginDef
import doobie.ConnectionIO
import zio.Task

import java.util.UUID

import zio.interop.catz.core._

@SuppressWarnings(Array("org.wartremover.warts.NonUnitStatements", "org.wartremover.warts.Null"))
object TestModule extends PluginDef {
  include(new MainModule[Task, UUID, ConnectionIO])
}
