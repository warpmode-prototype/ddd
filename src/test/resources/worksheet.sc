import cats.data.{EitherNec, NonEmptyList}
import cats.implicits._
import com.example.ddd.domain.catalog.CatalogPart.{Description, PartNumber}
import com.example.ddd.domain.order.LineItem.Quantity
import com.example.ddd.domain.order.{LineItem, PurchaseOrder}
import com.example.ddd.domain.{ApprovedLimit, DomainError, Money}
import com.example.ddd.api.json._
import eu.timepit.refined.auto._
import io.circe.literal._
import io.circe.refined._
import io.circe.syntax._

import java.util.{Currency, UUID}

def EUR(amount: BigDecimal): Money["EUR"] = Money.unsafeFrom["EUR"](amount)
def USD(amount: BigDecimal): Money["USD"] = Money.unsafeFrom["USD"](amount)
def MUR(amount: BigDecimal): Money["MUR"] = Money.unsafeFrom["MUR"](amount)

def createLineItem[CurrencyCode <: String: ValueOf](
  partNumber: String,
  description: String,
  quantity: Int,
  price: Money[CurrencyCode],
): EitherNec[DomainError, LineItem[CurrencyCode]] =
  (
    PartNumber.from(partNumber).toEitherNec,
    Description.from(description).toEitherNec,
    Quantity.from(quantity).toEitherNec,
  ).parMapN(LineItem(_, _, _, price))

@SuppressWarnings(Array("all"))
def unsafe[E, A](e: Either[E, A]): A = e.toOption.get

Description.unsafeFrom("yeah")
"description": Description

//Seq.empty[LineItem["EUR"]]: LineItems["EUR"]

List(MUR(1000), USD(1000), EUR(42))

Money.from["fred"](42)
//Money.unsafeFrom["fred"](42)

val rupies = "MUR"
Money.from[rupies.type](42)

Currency.getInstance("EUR") == Currency.getInstance("EUR")

USD(2) > USD(1)
EUR(3) == EUR(3)
USD(3) == EUR(3)
EUR(3).currencyCode

List(EUR(42), MUR(42).asInstanceOf[Money["EUR"]]).combineAll

val id      = PurchaseOrder.Id(UUID.randomUUID())
val version = 1

val limit            = unsafe(ApprovedLimit.from(EUR(300)))
val limitLow         = unsafe(ApprovedLimit.from(EUR(30)))
val limitBadCurrency = unsafe(ApprovedLimit.from(USD(1000)))

createLineItem("", "", -2, EUR(9.90))
LineItem("r2d2", "Item 1", 2, EUR(9.90))

val lineItems: NonEmptyList[LineItem["EUR"]] =
  NonEmptyList.fromListUnsafe(
    List(
      LineItem("r2d2", "Item 1", 2, EUR(9.95)),
      LineItem("r2d2", "Item 2", 1, EUR(19.95)),
    )
  )

//val lineItemsBad: NonEmptyList[LineItem["EUR"]] =
//  NonEmptyList.fromListUnsafe(
//    List(
//      LineItem("Item 1", 2, EUR(9.95)),
//      LineItem("Item 2", 1, USD(19.95)),
//    )
//  )

//PurchaseOrder(id, version, limitBadCurrency, lineItems)
val po1 = PurchaseOrder(id, version, limitLow, lineItems)
val po2 = PurchaseOrder(id, version, limit, lineItems)

// JSON

val descriptionJson = json""""""""
descriptionJson.as[Description]

val money     = EUR(42)
val moneyJson = money.asJson
moneyJson.as[Money[_ <: String]]
moneyJson.as[Money["EUR"]]

val approvedLimitJson = json"""{"amount" : 300,"currency" : "EUR"}"""
approvedLimitJson.as[ApprovedLimit[_ <: String]]
approvedLimitJson.as[ApprovedLimit["EUR"]]

val lineItem     = LineItem("r2d2", "Item 1", 2, EUR(9.90))
val lineItemJson = lineItem.asJson
lineItemJson.as[LineItem[_ <: String]]
lineItemJson.as[LineItem["EUR"]]

val lineItemsJson = lineItems.asJson
lineItemsJson.as[NonEmptyList[LineItem["EUR"]]]

val emptyLineItemsJson = json"""[]"""
emptyLineItemsJson.as[NonEmptyList[LineItem["EUR"]]]

val multiCurLineItemsJson =
  json"""[{"description":"Item 1","quantity":2,"price":{"amount":9.95,"currency":"EUR"}},{"description":"Item 2","quantity":1,"price":{"amount":19.95,"currency":"USD"}}]"""

multiCurLineItemsJson.as[NonEmptyList[LineItem["EUR"]]]

val poJson = unsafe(po2).asJson
poJson.as[PurchaseOrder[UUID, _ <: String]]
val code = "EUR"
poJson.as[PurchaseOrder[UUID, code.type]]

val multiCurPoJson =
  json"""{"id":"5ac93409-561e-45df-be6f-c3c8dab334c8","version":1,"approvedLimit":{"amount":300,"currency":"EUR"},"lineItems":[{"description":"Item 1","quantity":2,"price":{"amount":9.95,"currency":"MUR"}},{"description":"Item2","quantity":1,"price":{"amount":19.95,"currency":"USD"}}]}"""

multiCurPoJson.as[PurchaseOrder[UUID, _ <: String]]
multiCurPoJson.as[PurchaseOrder[UUID, "EUR"]]

val exceedingLimitPoJson =
  json"""{"id":"5ac93409-561e-45df-be6f-c3c8dab334c8","version":1,"approvedLimit":{"amount":30,"currency":"EUR"},"lineItems":[{"description":"Item 1","quantity":2,"price":{"amount":9.95,"currency":"EUR"}},{"description":"Item2","quantity":1,"price":{"amount":19.95,"currency":"EUR"}}]}"""

exceedingLimitPoJson.as[PurchaseOrder[UUID, "EUR"]]

val m1                           = EUR(42)
val m2                           = USD(43)
val mx                           = List(m1, m2)
val ml: List[Money[_ <: String]] = List(m1, m2)
