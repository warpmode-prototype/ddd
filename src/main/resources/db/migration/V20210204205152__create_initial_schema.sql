create table purchase_order
(
    id                    uuid default uuid_generate_v4() not null
        constraint purchase_order_pk
            primary key,
    version               bigint                          not null,
    currency              char(3)                         not null,
    approved_limit_amount numeric(15, 4)                  not null
);

INSERT INTO purchase_order (id, version, currency, approved_limit_amount)
VALUES ('8115ae0b-8d72-4727-8cc1-9b88e350276e', 1, 'EUR', 42.4300);
INSERT INTO purchase_order (id, version, currency, approved_limit_amount)
VALUES ('7c3603ad-9b43-4d92-8fac-7f1cda73f677', 1, 'EUR', 1000.0000);
INSERT INTO purchase_order (id, version, currency, approved_limit_amount)
VALUES ('84d6b385-7f9f-4c4a-9e31-1292f3a67305', 1, 'EUR', 1000.0000);

create table line_item
(
    purchase_order_id uuid           not null
        constraint line_item_purchase_order_id_fk
            references purchase_order
            on delete cascade,
    part_number       text           not null,
    description       text           not null,
    quantity          integer        not null,
    price             numeric(15, 4) not null,
    constraint line_item_pk
        unique (purchase_order_id, part_number)
);

create index line_item_part_number_index
    on line_item (part_number);

INSERT INTO line_item (purchase_order_id, part_number, description, quantity, price)
VALUES ('8115ae0b-8d72-4727-8cc1-9b88e350276e', 'pt394', 'Some other part', 3, 1.9500);
INSERT INTO line_item (purchase_order_id, part_number, description, quantity, price)
VALUES ('8115ae0b-8d72-4727-8cc1-9b88e350276e', 'pt395', 'Some Part', 2, 3.9900);
INSERT INTO line_item (purchase_order_id, part_number, description, quantity, price)
VALUES ('7c3603ad-9b43-4d92-8fac-7f1cda73f677', 'pt123', 'Description of part pt123', 4, 19.9900);
INSERT INTO line_item (purchase_order_id, part_number, description, quantity, price)
VALUES ('7c3603ad-9b43-4d92-8fac-7f1cda73f677', 'pt234', 'Description of part pt234', 7, 19.9900);
INSERT INTO line_item (purchase_order_id, part_number, description, quantity, price)
VALUES ('84d6b385-7f9f-4c4a-9e31-1292f3a67305', 'pt123', 'Description of part pt123', 4, 19.9900);
INSERT INTO line_item (purchase_order_id, part_number, description, quantity, price)
VALUES ('84d6b385-7f9f-4c4a-9e31-1292f3a67305', 'pt234', 'Description of part pt234', 7, 19.9900);
