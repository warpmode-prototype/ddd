package com.example.ddd

import api.ApiConfiguration
import application.ApplicationConfiguration
import domain.{ApprovedLimit, DomainConfiguration, Money}
import infrastructure.InfrastructureConfiguration

import cats.implicits._
import eu.timepit.refined.pureconfig._
import pureconfig.error.CannotConvert
import pureconfig.{ConfigReader, ConfigSource}
import pureconfig.generic.auto._

final case class MainConfiguration(
  api: ApiConfiguration,
  application: ApplicationConfiguration.type,
  domain: DomainConfiguration,
  infrastructure: InfrastructureConfiguration,
)

object MainConfiguration {

  implicit def moneyConfigReader[CurrencyCode <: String: ValueOf]: ConfigReader[Money[CurrencyCode]] =
    ConfigReader[BigDecimal].map(amount => Money.unsafeFrom[CurrencyCode](amount))

  implicit def approvedLimitConfigReader[CurrencyCode <: String: ValueOf]: ConfigReader[ApprovedLimit[CurrencyCode]] =
    ConfigReader[Money[CurrencyCode]].emap(money =>
      ApprovedLimit.from(money).leftMap(invalid => CannotConvert(money.toString, "ApprovedLimit", invalid.error))
    )

  def loadOrThrow: MainConfiguration = ConfigSource.default.loadOrThrow[MainConfiguration]
}
