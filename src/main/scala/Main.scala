package com.example.ddd

import api.json.key._
import infrastructure.UncaughtExceptionLogging
import infrastructure.persistence.DatabaseMigration

import distage.Injector
import doobie.ConnectionIO
import izumi.distage.framework.CoreCheckableAppSimple
import izumi.distage.model.definition.ModuleBase
import izumi.distage.model.plan.Roots
import org.http4s.server.Server
import zio.console.Console
import zio.interop.catz.core._
import zio.{ExitCode, Task, URIO, ZIO}

import java.util.UUID

object Main extends CoreCheckableAppSimple[Task] with zio.App with UncaughtExceptionLogging {
  private val injector = Injector[Task]()

  override def run(args: List[String]): URIO[Console, ExitCode] = (migrateDatabase *> startHttpServer).exitCode

  private def migrateDatabase: Task[Unit] =
    injector.produceRun(module)((databaseMigration: DatabaseMigration[Task]) => databaseMigration.run)

  private def startHttpServer: Task[Server[Task]] =
    injector.produceRun(module)((_: Server[Task]) => ZIO.never)

  override def module: ModuleBase = new MainModule[Task, UUID, ConnectionIO]

  override def roots: Roots = Roots.target[DatabaseMigration[Task]] ++ Roots.target[Server[Task]]
}
