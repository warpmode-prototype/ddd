package com.example.ddd

import api.{ApiConfiguration, ApiModule}
import application.{ApplicationConfiguration, ApplicationModule}
import domain.{DomainConfiguration, DomainModule}
import infrastructure.{InfrastructureConfiguration, InfrastructureModule}

import cats.Monad
import cats.effect.Sync
import distage.{ModuleDef, Tag, TagK}
import io.circe.Codec
import zio.BuildInfo

@SuppressWarnings(Array("org.wartremover.warts.NonUnitStatements"))
final class MainModule[F[_]: TagK: Monad, Key: Tag: Codec, TX[_]: TagK] extends ModuleDef {
  include(new ApiModule[F, Key, TX](BuildInfo.version))
  include(new ApplicationModule[F, Key, TX])
  include(new DomainModule[F, Key, TX])
  include(new InfrastructureModule[F])

  make[MainConfiguration].fromEffect((sync: Sync[F]) => sync.delay(MainConfiguration.loadOrThrow))
  make[ApiConfiguration].from((configuration: MainConfiguration) => configuration.api)
  make[ApplicationConfiguration.type].from((configuration: MainConfiguration) => configuration.application)
  make[DomainConfiguration].from((configuration: MainConfiguration) => configuration.domain)
  make[InfrastructureConfiguration].from((configuration: MainConfiguration) => configuration.infrastructure)
}
